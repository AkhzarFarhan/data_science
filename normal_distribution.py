import random
import matplotlib.pyplot as plt

base_array = []
for i in range(10):
    sub_array = []
    for j in range(100):
        sub_array.append(random.randint(1, 6))
    base_array.append(sum(sub_array))
x_array = [i for i in range(100, 601)]
y_array = []
for i in x_array:
    y_array.append(base_array.count(i)/len(base_array))
plt.plot(x_array, y_array, 'g')
plt.axis([min(x_array), max(x_array), min(y_array), max(y_array)])
plt.show()
# print(y_array)
# print(base_array)